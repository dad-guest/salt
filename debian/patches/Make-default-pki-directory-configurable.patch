From 7de9a5ae258b373a12903d17238eff2ee843a7e7 Mon Sep 17 00:00:00 2001
From: Benjamin Drung <benjamin.drung@profitbricks.com>
Date: Tue, 20 Feb 2018 11:27:16 +0100
Subject: [PATCH] Make default pki directory configurable

The files in /etc/salt/pki are not configuration files in the sense
of the FHS ("local file used to control the operation of a program").
Debian wants to change the default location to /var/lib/salt/pki (to
properly follow FHS and to allow setting StateDirectory in the salt
master systemd configuration).

Therefore introduce a STATE_DIR syspaths variable which defaults to
CONFIG_DIR, but can be individually customized.

fixes #3396
Bug-Debian: https://bugs.debian.org/698898
Forwarded: https://github.com/saltstack/salt/pull/46129
---
 salt/config/__init__.py | 6 +++---
 salt/syspaths.py        | 6 +++++-
 setup.py                | 5 +++++
 3 files changed, 13 insertions(+), 4 deletions(-)

diff --git a/salt/config/__init__.py b/salt/config/__init__.py
index 236cd3ca81..fcb2adb822 100644
--- a/salt/config/__init__.py
+++ b/salt/config/__init__.py
@@ -1112,7 +1112,7 @@ DEFAULT_MINION_OPTS = {
     'syndic_finger': '',
     'user': salt.utils.get_user(),
     'root_dir': salt.syspaths.ROOT_DIR,
-    'pki_dir': os.path.join(salt.syspaths.CONFIG_DIR, 'pki', 'minion'),
+    'pki_dir': os.path.join(salt.syspaths.STATE_DIR, 'pki', 'minion'),
     'id': '',
     'cachedir': os.path.join(salt.syspaths.CACHE_DIR, 'minion'),
     'append_minionid_config_dirs': [],
@@ -1375,7 +1375,7 @@ DEFAULT_MASTER_OPTS = {
     'keep_jobs': 24,
     'archive_jobs': False,
     'root_dir': salt.syspaths.ROOT_DIR,
-    'pki_dir': os.path.join(salt.syspaths.CONFIG_DIR, 'pki', 'master'),
+    'pki_dir': os.path.join(salt.syspaths.STATE_DIR, 'pki', 'master'),
     'key_cache': '',
     'cachedir': os.path.join(salt.syspaths.CACHE_DIR, 'master'),
     'file_roots': {
@@ -1688,7 +1688,7 @@ DEFAULT_PROXY_MINION_OPTS = {
 
     'proxy_keep_alive': True,  # by default will try to keep alive the connection
     'proxy_keep_alive_interval': 1,  # frequency of the proxy keepalive in minutes
-    'pki_dir': os.path.join(salt.syspaths.CONFIG_DIR, 'pki', 'proxy'),
+    'pki_dir': os.path.join(salt.syspaths.STATE_DIR, 'pki', 'proxy'),
     'cachedir': os.path.join(salt.syspaths.CACHE_DIR, 'proxy'),
     'sock_dir': os.path.join(salt.syspaths.SOCK_DIR, 'proxy'),
 }
diff --git a/salt/syspaths.py b/salt/syspaths.py
index f39380b170..3c39b6cf5e 100644
--- a/salt/syspaths.py
+++ b/salt/syspaths.py
@@ -33,7 +33,7 @@ except ImportError:
     import types
     __generated_syspaths = types.ModuleType('salt._syspaths')
     for key in ('ROOT_DIR', 'CONFIG_DIR', 'CACHE_DIR', 'SOCK_DIR',
-                'SRV_ROOT_DIR', 'BASE_FILE_ROOTS_DIR',
+                'SRV_ROOT_DIR', 'STATE_DIR', 'BASE_FILE_ROOTS_DIR',
                 'BASE_PILLAR_ROOTS_DIR', 'BASE_THORIUM_ROOTS_DIR',
                 'BASE_MASTER_ROOTS_DIR', 'LOGS_DIR', 'PIDFILE_DIR',
                 'SPM_FORMULA_PATH', 'SPM_PILLAR_PATH', 'SPM_REACTOR_PATH',
@@ -100,6 +100,10 @@ SRV_ROOT_DIR = __generated_syspaths.SRV_ROOT_DIR
 if SRV_ROOT_DIR is None:
     SRV_ROOT_DIR = os.path.join(ROOT_DIR, 'srv')
 
+STATE_DIR = __generated_syspaths.STATE_DIR
+if STATE_DIR is None:
+    STATE_DIR = CONFIG_DIR
+
 BASE_FILE_ROOTS_DIR = __generated_syspaths.BASE_FILE_ROOTS_DIR
 if BASE_FILE_ROOTS_DIR is None:
     BASE_FILE_ROOTS_DIR = os.path.join(SRV_ROOT_DIR, 'salt')
diff --git a/setup.py b/setup.py
index 344aa78d92..0f6de5c567 100755
--- a/setup.py
+++ b/setup.py
@@ -231,6 +231,7 @@ class GenerateSaltSyspaths(Command):
                 cache_dir=self.distribution.salt_cache_dir,
                 sock_dir=self.distribution.salt_sock_dir,
                 srv_root_dir=self.distribution.salt_srv_root_dir,
+                state_dir=self.distribution.salt_state_dir,
                 base_file_roots_dir=self.distribution.salt_base_file_roots_dir,
                 base_pillar_roots_dir=self.distribution.salt_base_pillar_roots_dir,
                 base_master_roots_dir=self.distribution.salt_base_master_roots_dir,
@@ -721,6 +722,7 @@ CONFIG_DIR = {config_dir!r}
 CACHE_DIR = {cache_dir!r}
 SOCK_DIR = {sock_dir!r}
 SRV_ROOT_DIR= {srv_root_dir!r}
+STATE_DIR = {state_dir!r}
 BASE_FILE_ROOTS_DIR = {base_file_roots_dir!r}
 BASE_PILLAR_ROOTS_DIR = {base_pillar_roots_dir!r}
 BASE_MASTER_ROOTS_DIR = {base_master_roots_dir!r}
@@ -866,6 +868,8 @@ class SaltDistribution(distutils.dist.Distribution):
          'Salt\'s pre-configured socket directory'),
         ('salt-srv-root-dir=', None,
          'Salt\'s pre-configured service directory'),
+        ('salt-state-dir=', None,
+         'Salt\'s pre-configured variable state directory (used for storing pki data)'),
         ('salt-base-file-roots-dir=', None,
          'Salt\'s pre-configured file roots directory'),
         ('salt-base-pillar-roots-dir=', None,
@@ -897,6 +901,7 @@ class SaltDistribution(distutils.dist.Distribution):
         self.salt_cache_dir = None
         self.salt_sock_dir = None
         self.salt_srv_root_dir = None
+        self.salt_state_dir = None
         self.salt_base_file_roots_dir = None
         self.salt_base_thorium_roots_dir = None
         self.salt_base_pillar_roots_dir = None
-- 
2.14.1

